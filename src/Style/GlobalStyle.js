import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import 'antd/dist/antd.css';

const GlobalStyle = createGlobalStyle`
  ${normalize}

  html,
  body {
    font-family:'Noto Sans KR', sans-serif, Roboto,'Helvetica Neue';
    font-size: 14px;
    overflow: hidden;
  }

  * {
    box-sizing: border-box;
  }
  
`;

export default GlobalStyle;
