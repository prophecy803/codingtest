import React, { useState } from 'react';
import { Modal } from 'antd';
import Details from './Details';

const InfoModal = props => {
  return (
    <>
      <Modal
        title={props?.selectedRow?.name}
        centered
        visible={props.isModalVisible}
        onOk={props.toggleModal}
        onCancel={props.toggleModal}
        width={700}>
        <Details beerData={props?.selectedRow} />
      </Modal>
    </>
  );
};

export default InfoModal;
