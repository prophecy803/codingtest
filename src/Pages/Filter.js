import styled from 'styled-components';
import { Select } from 'antd';

const Filter = props => {
  const { Option } = Select;

  function handleChange(e) {
    props.setOptionState(e);
  }

  return (
    <SelectWrapper>
      <SelectContainer
        showArrow
        mode="multiple"
        placeholder="Select abv"
        onChange={handleChange}>
        {props.ALL_OPTIONS.map(([min, max], i) => {
          return (
            <Option key={i}>
              {min}~{max}
            </Option>
          );
        })}
      </SelectContainer>
    </SelectWrapper>
  );
};

const SelectWrapper = styled.div`
  .ant-select-multiple .ant-select-selector {
    white-space: nowrap;
    overflow: auto;
    height: 35px;
  }
`;

const SelectContainer = styled(Select)`
  width: 200px;
  margin: 0;
  height: auto;
`;

export default Filter;
