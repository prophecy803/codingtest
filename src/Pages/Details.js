import React from 'react';
import styled from 'styled-components';

const Details = ({ beerData }) => {
  return (
    <Table>
      <tbody>
        <tr>
          <td>id</td>
          <td>{beerData.id}</td>
        </tr>
        <tr>
          <td>tagline</td>
          <td>{beerData.tagline}</td>
        </tr>
        <tr>
          <td>first brewed</td>
          <td>{beerData.first_brewed}</td>
        </tr>
        <tr>
          <td>description</td>
          <td>{beerData.description}</td>
        </tr>
        <tr>
          <td>abv</td>
          <td>{beerData.abv}</td>
        </tr>
        <tr>
          <td>ibu</td>
          <td>{beerData.ibu}</td>
        </tr>
        <tr>
          <td>target fg</td>
          <td>{beerData.target_fg}</td>
        </tr>
        <tr>
          <td>target og</td>
          <td>{beerData.target_og}</td>
        </tr>
        <tr>
          <td>ebc</td>
          <td>{beerData.ebc}</td>
        </tr>
        <tr>
          <td>srm</td>
          <td>{beerData.srm}</td>
        </tr>
        <tr>
          <td>ph</td>
          <td>{beerData.ph}</td>
        </tr>
        <tr>
          <td>attenuation level</td>
          <td>{beerData.attenuation_level}</td>
        </tr>
        <tr>
          <td>volume</td>
          <td>
            {beerData.volume.value} {beerData.volume.unit}
          </td>
        </tr>
        <tr>
          <td>boil volume</td>
          <td>
            {beerData.boil_volume.value} {beerData.boil_volume.unit}
          </td>
        </tr>
        <tr>
          <td>method</td>

          <p>mash temp</p>
          <ul>
            <p>
              value: {beerData.method.mash_temp[0].temp.value}{' '}
              {beerData.method.mash_temp[0].temp.unit}
            </p>
            <p>duration: {beerData.method.mash_temp[0].duration}</p>
          </ul>
        </tr>
        <tr>
          <td>fermentation</td>
          <td>
            temp: {beerData.method.fermentation.temp.value}{' '}
            {beerData.method.fermentation.temp.unit}
          </td>
        </tr>
        <tr>
          <td>twist</td>
          <td>{beerData.method.fermentation.twist}</td>
        </tr>
        <tr>
          <td>ingredients</td>
          <td>
            <p>malt</p>
            <ul>
              {beerData.ingredients.malt.map(v => {
                return (
                  <p>
                    {v.name} {v.amount.value} {v.amount.unit}
                  </p>
                );
              })}
            </ul>
            <p>hops</p>
            <ul>
              {beerData.ingredients.hops.map(v => {
                return (
                  <p>
                    {v.name} {v.amount.value} {v.amount.unit}
                  </p>
                );
              })}
            </ul>
          </td>
        </tr>

        <tr>
          <td>yeast</td>
          <td>{beerData.ingredients.yeast}</td>
        </tr>
        <tr>
          <td>food pairing</td>
          <td>{beerData.food_pairing.join(', ')}</td>
        </tr>
        <tr>
          <td>brewers tips</td>
          <td>{beerData.brewers_tips}</td>
        </tr>
        <tr>
          <td>contributed by</td>
          <td>{beerData.contributed_by}</td>
        </tr>
      </tbody>
    </Table>
  );
};

const Table = styled.table`
  font-size: 12px;
  border: 1px solid black;
  td,
  tr {
    border: 1px solid black;
  }
`;
export default Details;
