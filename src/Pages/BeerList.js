import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import MaterialTable, { MTableToolbar } from 'material-table';
import { Spin, Button } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import Filter from './Filter';
import useFetch from '../Hook/useFetch';
import tableIcons from '../Style/tableIcons';
import PatchedPagination from '../PatchedPagination';
import InfoModal from './InfoModal';

const BeerList = () => {
  const { data, loading, error } = useFetch('https://api.punkapi.com/v2/beers');
  const mainColumns = ['id', 'name', 'image_url', 'abv', 'ibu', 'ebc', 'ph'];

  const [ColoredRow, setColoredRow] = useState(null);
  const [ClickedRow, setClickedRow] = useState(null);
  const [options, setOptions] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const ALL_OPTIONS = [
    [1, 10],
    [11, 20],
    [21, 30],
    [31, 40],
    [41, 50],
    [51, 60],
  ];

  const setOptionState = e => {
    setOptions([...options, ALL_OPTIONS[e]]);
  };

  const MakeRows = filteredData => {
    return filteredData?.map(v => {
      return {
        id: v.id,
        name: v.name,
        image_url: v.image_url,
        abv: v.abv,
        ibu: v.ibu,
        ebc: v.ebc,
        ph: v.ph,
      };
    });
  };

  const filteredData = () => {
    if (options?.length === 0) {
      return MakeRows(data);
    }
    const filteredRow = options?.reduce((acc, [min, max]) => {
      return acc.concat(data.filter(v => v.abv >= min && v.abv <= max));
    }, []);
    return MakeRows(
      filteredRow.sort((a, b) => {
        return a.abv - b.abv;
      })
    );
  };

  const columnHeaders = () => {
    const data =
      localStorage.getItem('headers') === null
        ? mainColumns
        : JSON.parse(localStorage.getItem('headers'));

    return data.map(v => {
      return {
        title: v,
        field: v,
        filtering: false,
        ...(v === 'image_url' && {
          title: 'image',
          render: rowData => (
            <img src={rowData.image_url} style={{ width: 15 }} />
          ),
        }),
        ...(v === 'abv' && {
          filtering: true,
        }),
      };
    });
  };
  const onColumnDragged = (sourceIndex, destinationIndex) => {
    mainColumns.splice(
      destinationIndex,
      0,
      mainColumns.splice(sourceIndex, 1)[0]
    );
    localStorage.setItem('headers', JSON.stringify(mainColumns));
  };

  const AbvFilter = () => {
    return (
      <AbvFilterContainer>
        {options.map(([min, max]) => {
          return (
            <span>
              {min}~{max}
            </span>
          );
        })}
      </AbvFilterContainer>
    );
  };

  const clearFilterOptions = () => {
    setOptions([]);
  };

  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const onRowClick = (evt, selectedRow) => {
    setClickedRow(selectedRow.id);
    setColoredRow(selectedRow.tableData.id);
    toggleModal();
  };

  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

  return (
    <div>
      <div style={{ maxWidth: '100%' }}>
        {error && <p>Your Request is failed.</p>}
        {loading && <Spin indicator={antIcon} />}

        {data && (
          <TableContainer>
            <MaterialTable
              icons={tableIcons}
              onRowClick={onRowClick}
              onColumnDragged={onColumnDragged}
              columns={columnHeaders()}
              data={filteredData()}
              title="Beer List"
              components={{
                Pagination: PatchedPagination,
                Toolbar: props => (
                  <>
                    <MTableToolbar {...props} />
                    <ToolbarContainer>
                      <Filter
                        ALL_OPTIONS={ALL_OPTIONS}
                        setOptionState={setOptionState}
                      />
                      <AbvFilter />
                      <Button onClick={clearFilterOptions} type="link">
                        Clear
                      </Button>
                    </ToolbarContainer>
                  </>
                ),
              }}
              options={{
                headerStyle: {
                  backgroundColor: '#D1F0D9',
                  color: '000000',
                  fontWeight: 700,
                  rowStyle: rowData => ({
                    backgroundColor:
                      ColoredRow === rowData.tableData.id ? '#eff1f4' : '#FFF',
                  }),
                },
              }}
            />
            <InfoModal
              toggleModal={toggleModal}
              isModalVisible={isModalVisible}
              selectedRow={data?.find(v => v.id === ClickedRow)}
            />
          </TableContainer>
        )}
      </div>
    </div>
  );
};

const ToolbarContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`;

const AbvFilterContainer = styled.div`
  display: flex;
  & > span {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin: 0 5px;
    padding: 4px;
    color: #1b365c;
    font-size: 12px;
    background-color: #d1f0d9;
    border-radius: 7px;
  }
`;

const TableContainer = styled.div`
  height: 100vh;
  overflow: scroll;
`;

export default BeerList;
