import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import Filter from './Filter';

const Home = () => {
  return (
    <div>
      <Link to="/beerlist">Check out our beer list!</Link>
    </div>
  );
};

export default Home;
